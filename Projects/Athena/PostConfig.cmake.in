#
# File taking care of pointing the downstream projects at the right
# version of the externals.
#

# Set the versions of the TDAQ projects:
set( TDAQ_PROJECT_NAME "@TDAQ_PROJECT_NAME@" CACHE STRING
   "Name of the tdaq project" )
set( TDAQ_VERSION "@TDAQ_VERSION@" CACHE STRING
   "The version of tdaq to use for the build" ) 
set( TDAQ_ATROOT "@TDAQ_ATROOT@" CACHE PATH
   "The directory to pick up tdaq from" )

set( TDAQ-COMMON_VERSION "@TDAQ-COMMON_VERSION@" CACHE STRING
   "The version of tdaq-common to use for the build" )
set( TDAQ-COMMON_ATROOT "@TDAQ-COMMON_ATROOT@" CACHE PATH
   "The directory to pick up tdaq-common from" )

set( DQM-COMMON_VERSION "@DQM-COMMON_VERSION@" CACHE STRING
   "The version of dqm-common to use for the build" )
set( DQM-COMMON_ATROOT "@DQM-COMMON_ATROOT@" CACHE PATH
   "The directory to pick up dqm-common from" )

# Find Gaudi:
if( Athena_FIND_QUIETLY )
   find_package( Gaudi REQUIRED QUIET )
else()
   find_package( Gaudi REQUIRED )
endif()

# Load all the files from the externals/ subdirectory:
get_filename_component( _thisdir ${CMAKE_CURRENT_LIST_FILE} PATH )
file( GLOB _externals "${_thisdir}/externals/*.cmake" )
unset( _thisdir )
foreach( _external ${_externals} )
   include( ${_external} )
   get_filename_component( _extName ${_external} NAME_WE )
   string( TOUPPER ${_extName} _extNameUpper )
   if( NOT Athena_FIND_QUIETLY )
      message( STATUS "Taking ${_extName} from: ${${_extNameUpper}_LCGROOT}" )
   endif()
   unset( _extName )
   unset( _extNameUpper )
endforeach()
unset( _external )
unset( _externals )
